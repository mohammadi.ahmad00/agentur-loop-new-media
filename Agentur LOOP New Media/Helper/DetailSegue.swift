//
//  DetailSegue.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/9/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

class DetailSegue: UIStoryboardSegue {
    
    override func perform() {
        scale()
    }
    
    private func scale() {
        let toVC = self.destination as! DetailVC
        let fromVC = self.source as! MainVC
        
        let containerView = fromVC.view.superview
        
        toVC.view.alpha = 0
        toVC.ivTop.alpha = 0
        containerView?.addSubview(toVC.view)
        
        MainVC.fakeBackView.addSubview(MainVC.fakeCloseBtn)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            MainVC.fakeView.frame = CGRect(x: containerView!.frame.midX - 85, y: 120, width: 170, height: 210)
            fromVC.viewTopBar.btnPlus.transform = CGAffineTransform(rotationAngle: .pi/4)
            MainVC.fakeCloseBtn.transform = CGAffineTransform(rotationAngle: .pi/4)
            MainVC.fakeBackView.alpha = 1
            MainVC.fakeView.layoutIfNeeded()
        }) { (done) in

            fromVC.navigationController?.pushViewController(toVC, animated: false)
            
            toVC.view.alpha = 1
            toVC.ivTop.alpha = 1
            toVC.btnClose.alpha = 0.6
            MainVC.fakeCloseBtn.removeFromSuperview()
            MainVC.fakeBackView.removeFromSuperview()
            MainVC.fakeView.removeFromSuperview()
            fromVC.viewTopBar.btnPlus.transform = CGAffineTransform.identity
        }
        
    }
}


class DetailUnwindSegue: UIStoryboardSegue {
    override func perform() {
        scale()
    }
    
    private func scale() {
        let fromVC = self.source as! DetailVC
        let toVC = self.destination as! MainVC
        
        let window = UIApplication.shared.keyWindow
        
        let containerView = fromVC.view.superview
        
        toVC.view.alpha = 0
        toVC.view.frame.origin.y += window?.safeAreaInsets.top ?? 0
        containerView?.addSubview(toVC.view)
        
        guard let selectedCell = fromVC.item.containerCollection?.cellForItem(at: fromVC.item.index) else {
            fromVC.navigationController?.popViewController(animated: false)
            return
        }
        selectedCell.alpha = 0
        
        let  destFrame = fromVC.item.frame
        var destFrameNoTopInset = destFrame
        destFrameNoTopInset.origin.y -= (window?.safeAreaInsets.top ?? 0)
        
        let whiteView = UIView(frame: destFrameNoTopInset)
        whiteView.backgroundColor = .white
        toVC.view.addSubview(whiteView)
        
        let fakeImage = UIImageView(image: fromVC.item.image)
        fakeImage.frame = fromVC.ivTop.frame
        fakeImage.contentMode = .scaleAspectFill
        fakeImage.clipsToBounds = true
        window?.addSubview(fakeImage)
        
        fromVC.ivTop.alpha = 0
//        toVC.viewTopBar.alpha = 0
        toVC.viewTopBar.btnPlus.transform = CGAffineTransform(rotationAngle: .pi/4)
        fromVC.btnClose.transform = CGAffineTransform(rotationAngle: .pi/4)
        
        UIView.animate(withDuration: 0.5, animations: {
            
            fakeImage.frame = destFrame
            toVC.view.alpha = 1
            fromVC.btnClose.transform = CGAffineTransform.identity
            toVC.viewTopBar.btnPlus.transform = CGAffineTransform.identity
            
        }) { (_) in
            
            fromVC.navigationController?.popViewController(animated: false)
            selectedCell.alpha = 1
            fakeImage.removeFromSuperview()
            whiteView.removeFromSuperview()
            
        }
        
        
    }
}

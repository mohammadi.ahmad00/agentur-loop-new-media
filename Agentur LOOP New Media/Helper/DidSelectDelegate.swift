//
//  DidSelectDelegate.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/9/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

protocol DidSelectDelegate {
    func didSelect(collection: UICollectionView, index: IndexPath, item: Item)
}

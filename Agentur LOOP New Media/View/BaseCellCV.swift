//
//  BaseCellCV.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/9/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

class BaseCellCV: UIView {
    
    private let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = .gray
        lbl.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let cv: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsHorizontalScrollIndicator = false
        cv.backgroundColor = .clear
        return cv
    }()
    
    init(title: String) {
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        lblTitle.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initView() {
        
        addSubview(lblTitle)
        addSubview(cv)
        
        NSLayoutConstraint.activate([
            lblTitle.topAnchor.constraint(equalTo: topAnchor),
            lblTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 14),
            lblTitle.rightAnchor.constraint(equalTo: rightAnchor),
            lblTitle.heightAnchor.constraint(equalToConstant: 30)
        ])
        
        NSLayoutConstraint.activate([
            cv.topAnchor.constraint(equalTo: lblTitle.bottomAnchor),
            cv.bottomAnchor.constraint(equalTo: bottomAnchor),
            cv.leftAnchor.constraint(equalTo: leftAnchor),
            cv.rightAnchor.constraint(equalTo: rightAnchor)
        ])
        
    }
    
}

//
//  TopBarView.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/8/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

protocol TopBarViewDelegate {
    func didTapOnSearchBtn()
    func didTapOnPlusBtn()
}

class TopBarView: UIView {
    
    var delegate: TopBarViewDelegate?
    
    private let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.text = "Discover"
        lbl.textAlignment = .center
        lbl.alpha = 0.7
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        lbl.adjustsFontSizeToFitWidth = true
        return lbl
    }()
    
    private let btnSearch: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "search"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.alpha = 0.6
        return btn
    }()
    
    let btnPlus: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "plus"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.alpha = 0.6
        return btn
    }()
    
    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        initView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initView() {
        
        btnSearch.addTarget(self, action: #selector(btnSearchAction), for: .touchUpInside)
        btnPlus.addTarget(self, action: #selector(btnPlusAction), for: .touchUpInside)
        
        addSubview(lblTitle)
        addSubview(btnSearch)
        addSubview(btnPlus)
        
        NSLayoutConstraint.activate([
            lblTitle.centerYAnchor.constraint(equalTo: centerYAnchor),
            lblTitle.centerXAnchor.constraint(equalTo: centerXAnchor),
            lblTitle.heightAnchor.constraint(equalTo: heightAnchor),
            lblTitle.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.7)
        ])
        
        NSLayoutConstraint.activate([
            btnSearch.centerYAnchor.constraint(equalTo: centerYAnchor),
            btnSearch.rightAnchor.constraint(equalTo: lblTitle.leftAnchor),
            btnSearch.widthAnchor.constraint(equalToConstant: 28),
            btnSearch.heightAnchor.constraint(equalTo: heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            btnPlus.centerYAnchor.constraint(equalTo: centerYAnchor),
            btnPlus.leftAnchor.constraint(equalTo: lblTitle.rightAnchor),
            btnPlus.heightAnchor.constraint(equalTo: heightAnchor),
            btnPlus.widthAnchor.constraint(equalToConstant: 20)
        ])
        
    }
    
    @objc private func btnSearchAction() {
        delegate?.didTapOnSearchBtn()
    }
    
    @objc private func btnPlusAction() {
        delegate?.didTapOnPlusBtn()
    }
    
    
}

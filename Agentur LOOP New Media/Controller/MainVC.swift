//
//  MainVC.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/8/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

class MainVC: UIViewController, DidSelectDelegate{
    
    static var fakeBackView : UIView = {
        let vi = UIView()
        vi.backgroundColor = .white
        vi.alpha = 0
        return vi
    }()
    
    static var fakeView = UIImageView()
    
    static var fakeCloseBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "plus"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.alpha = 0.6
        return btn
    }()
    
    private var topViewMaxHeight: CGFloat = 34
    private var topViewHeightConstraint: NSLayoutConstraint!
    let viewTopBar: TopBarView = {
        let vi = TopBarView()
        vi.translatesAutoresizingMaskIntoConstraints = false
        return vi
    }()
    
    private let cvContainerCellIdentifier = "cvContainerCellIdentifier"
    private let cvContainer: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 15
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.showsVerticalScrollIndicator = false
        return cv
    }()
    
    let viBanner = BannerView()
    let viVertical = VerticalCellCV()
    let viHorizontal = HorizontalCellCV()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func initView() {
        
        view.backgroundColor = .white
        
            // Title Label Setup
        view.addSubview(viewTopBar)
        NSLayoutConstraint.activate([
            viewTopBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            viewTopBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            viewTopBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 28)
        ])
        topViewHeightConstraint = viewTopBar.heightAnchor.constraint(equalToConstant: 30)
        topViewHeightConstraint.isActive = true
        
            // Collection Setup
        cvContainer.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cvContainerCellIdentifier)
        cvContainer.delegate = self
        cvContainer.dataSource = self
        view.addSubview(cvContainer)
        
        NSLayoutConstraint.activate([cvContainer.topAnchor.constraint(equalTo: viewTopBar.bottomAnchor, constant: 10),
                                     cvContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                                     cvContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     cvContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        viBanner.delegate = self
        viVertical.delegate = self
        viHorizontal.delegate = self
        
    }
 
    @objc private func navBarButtonAction() {
        print("1234")
    }
    
}

    // MARK:- Collection Delegate & DataSource
extension MainVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cvContainerCellIdentifier, for: indexPath)
        switch indexPath.item {
        case 0:
            if !cell.contentView.subviews.contains(viBanner) {
                cell.contentView.subviews.forEach { (vi) in
                    vi.removeFromSuperview()
                }
                cell.contentView.addSubview(viBanner)
                NSLayoutConstraint.activate([
                    viBanner.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                    viBanner.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                    viBanner.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                    viBanner.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor)
                ])
            }
            return cell
        case 1:
            if !cell.contentView.subviews.contains(viVertical) {
                cell.contentView.subviews.forEach { (vi) in
                    vi.removeFromSuperview()
                }
                cell.contentView.addSubview(viVertical)
                NSLayoutConstraint.activate([
                    viVertical.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                    viVertical.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                    viVertical.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                    viVertical.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor)
                ])
            }
            return cell
        case 2:
        if !cell.contentView.subviews.contains(viHorizontal) {
            cell.contentView.subviews.forEach { (vi) in
                vi.removeFromSuperview()
            }
            cell.contentView.addSubview(viHorizontal)
            NSLayoutConstraint.activate([
                viHorizontal.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                viHorizontal.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                viHorizontal.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                viHorizontal.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor)
            ])
        }
        return cell
        default:
            return cell
        }

        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.item {
        case 0:
            return CGSize(width: collectionView.frame.width, height: 180)
        case 1:
            return CGSize(width: collectionView.frame.width, height: 200)
        case 2:
            return CGSize(width: collectionView.frame.width, height: 160)
        default:
            return CGSize(width: collectionView.frame.width, height: 200)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = topViewMaxHeight - scrollView.contentOffset.y
        print(offset)
        if CGFloat(0) <= offset && offset <= topViewMaxHeight {
            topViewHeightConstraint.constant = offset
            UIView.animate(withDuration: 0.3) {[weak self] in
                guard let sSelf = self else {
                    return
                }
                sSelf.view.layoutIfNeeded()
            }
        } else if offset < 0 {
            topViewHeightConstraint.constant = 0
            UIView.animate(withDuration: 0.08) {[weak self] in
                guard let sSelf = self else {
                    return
                }
                sSelf.view.layoutIfNeeded()
            }
        } else if offset > topViewMaxHeight {
            topViewHeightConstraint.constant = topViewMaxHeight
            UIView.animate(withDuration: 0.3) {[weak self] in
                guard let sSelf = self else {
                    return
                }
                sSelf.view.layoutIfNeeded()
            }
        }
        
    }
    
    func didSelect(collection: UICollectionView, index: IndexPath, item: Item) {
        let cell = collection.cellForItem(at: index)
        let loc = cell!.convert(cell!.bounds, to: view)
        
        MainVC.fakeBackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        view.addSubview(MainVC.fakeBackView)
        
        MainVC.fakeBackView.alpha = 0
        
        MainVC.fakeView.image = item.image
        MainVC.fakeView.contentMode = .scaleAspectFill
        MainVC.fakeView.clipsToBounds = true
        MainVC.fakeView.frame = loc
        view.addSubview(MainVC.fakeView)
        
        
        let closeBtnLoc = viewTopBar.btnPlus.convert(viewTopBar.btnPlus.bounds, to: view)
        MainVC.fakeCloseBtn.frame = closeBtnLoc
        let itm = item
        itm.frame = cell!.convert(cell!.bounds, to: UIApplication.shared.keyWindow)
        performSegue(item: itm, closeBtnFrame: closeBtnLoc)
    }
    
    private func performSegue(item:Item, closeBtnFrame: CGRect) {
        
        let segue = DetailSegue(identifier: nil, source: self, destination: DetailVC(item: item, closeBtnFrame: closeBtnFrame))
        segue.perform()
        
    }
    
}
    // MARK:- Top View Delegate
extension MainVC: TopBarViewDelegate {
    func didTapOnSearchBtn() {
        print("Search Btn Tapped")
    }
    
    func didTapOnPlusBtn() {
        print("Plus Btn Tapped")
    }
    
    
}

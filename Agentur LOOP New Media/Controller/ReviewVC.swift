//
//  ReviewVC.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/12/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

class ReviewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var reviews = [Review]()
    
    private let btnClose: UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "plus"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        btn.alpha = 0.6
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.alpha = 0.6
        lbl.textColor = .black
        lbl.textAlignment = .left
        lbl.text = "Reviews (1458)"
        lbl.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        return lbl
    }()
    
    static let tblCellIdentifier = "tblCellIdentifier"
    private let tbl: UITableView = {
        let tbl = UITableView(frame: .zero, style: .plain)
        tbl.translatesAutoresizingMaskIntoConstraints = false
        return tbl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reviews = MockData.mockReviews
        initView()
    }
    
    private func initView() {
        
        view.backgroundColor = .white
        
        view.addSubview(btnClose)
        view.addSubview(lblTitle)
        
        tbl.register(ReviewCell.self, forCellReuseIdentifier: ReviewVC.tblCellIdentifier)
        tbl.delegate = self
        tbl.dataSource = self
        view.addSubview(tbl)
        
        NSLayoutConstraint.activate([
            btnClose.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12),
            btnClose.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30),
            btnClose.widthAnchor.constraint(equalToConstant: 20),
            btnClose.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        NSLayoutConstraint.activate([
            lblTitle.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12),
            lblTitle.rightAnchor.constraint(equalTo: btnClose.leftAnchor, constant: -8),
            lblTitle.topAnchor.constraint(equalTo: btnClose.topAnchor),
            lblTitle.heightAnchor.constraint(equalTo: btnClose.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            tbl.leftAnchor.constraint(equalTo: view.leftAnchor),
            tbl.rightAnchor.constraint(equalTo: view.rightAnchor),
            tbl.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 8),
            tbl.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        btnClose.transform = CGAffineTransform(rotationAngle: .pi/4)
        btnClose.addTarget(self, action: #selector(btnCloseAction), for: .touchUpInside)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewVC.tblCellIdentifier, for: indexPath) as! ReviewCell
        cell.commentCalculatedHeight = 90 + reviews[indexPath.row].descNeededHeight(width: tableView.frame.width - 16)
        cell.review = reviews[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = 90 + reviews[indexPath.row].descNeededHeight(width: tableView.frame.width - 16)
        return height
    }
    
    @objc private func btnCloseAction() {
        dismiss(animated: true, completion: nil)
    }
    
}

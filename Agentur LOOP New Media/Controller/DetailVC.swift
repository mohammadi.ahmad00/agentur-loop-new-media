//
//  DetailVC.swift
//  Agentur LOOP New Media
//
//  Created by ahmad mohammadi on 9/10/20.
//  Copyright © 2020 AhmadMohammadi. All rights reserved.
//

import UIKit

class DetailVC: UIViewController, CustomButtonDelegate {
    
    var item: Item
    
    var ivTop: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    private let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        lbl.textColor = .black
        lbl.alpha = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let lblSubTitle: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.font = UIFont.systemFont(ofSize: 15, weight: .light)
        lbl.textColor = .gray
        lbl.alpha = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private var viRating = RatingView()
    
    private let lblDescription: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        lbl.textColor = .gray
        lbl.numberOfLines = 0
        lbl.alpha = 0
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private let btnMore = CustomButton(frame: .zero)
    private var btnMoreBottomConstraint: NSLayoutConstraint!
    
    let btnClose : UIButton = {
        let btn = UIButton(type: .system)
        btn.setImage(UIImage(named: "plus"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    
    init(item: Item, closeBtnFrame: CGRect) {
        self.item = item
        self.item.rate = (String(format: "%.1f", self.item.rate) as NSString).doubleValue
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
        ivTop.frame = CGRect(x: view.frame.midX - 85, y: 120, width: 170, height: 210)
        btnClose.frame = closeBtnFrame
        view.addSubview(ivTop)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initView()
    }
    
    private func initView() {
        
        ivTop.image = item.image
        lblTitle.text = item.title
        lblSubTitle.text = item.subtitle
        lblDescription.text = item.desc
        viRating.rating = item.rate
        
        viRating.alpha = 0
        
        btnClose.transform = CGAffineTransform(rotationAngle: .pi/4)
        
        view.addSubview(lblTitle)
        view.addSubview(lblSubTitle)
        view.addSubview(viRating)
        view.addSubview(lblDescription)
        view.addSubview(btnMore)
        view.addSubview(btnClose)
        
        NSLayoutConstraint.activate([
            lblTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lblTitle.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            lblTitle.topAnchor.constraint(equalTo: ivTop.bottomAnchor, constant: 12),
            lblTitle.heightAnchor.constraint(lessThanOrEqualToConstant: 50),
        ])
        
        NSLayoutConstraint.activate([
            lblSubTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lblSubTitle.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            lblSubTitle.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: 8),
            lblSubTitle.heightAnchor.constraint(lessThanOrEqualToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            viRating.topAnchor.constraint(equalTo: lblSubTitle.bottomAnchor, constant: 12),
            viRating.heightAnchor.constraint(equalToConstant: 28),
            viRating.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            viRating.widthAnchor.constraint(equalToConstant: 146)
        ])

        NSLayoutConstraint.activate([
            btnMore.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            btnMore.heightAnchor.constraint(equalToConstant: 42),
            btnMore.widthAnchor.constraint(equalToConstant: 220)
        ])
        
        NSLayoutConstraint.activate([
            lblDescription.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            lblDescription.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85),
            lblDescription.topAnchor.constraint(equalTo: viRating.bottomAnchor, constant: 30)
        ])
        
        let descBottomConstraint = lblDescription.bottomAnchor.constraint(lessThanOrEqualTo: btnMore.topAnchor, constant: -10)
        descBottomConstraint.priority = UILayoutPriority(rawValue: 1000)
        descBottomConstraint.isActive = true
        
        let descHeighConstraint = lblDescription.heightAnchor.constraint(greaterThanOrEqualToConstant: 20)
        descHeighConstraint.priority = UILayoutPriority(rawValue: 500)
        descHeighConstraint.isActive = true
        
        btnMoreBottomConstraint = btnMore.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8)
        btnMoreBottomConstraint.isActive = true
        
        btnMore.alpha = 0
        btnMore.layoutIfNeeded()
        
        showImageShadow()
        
        btnMore.delegate = self
        
        btnClose.addTarget(self, action: #selector(closeBtnAction), for: .touchUpInside)
        
    }
    
    private func showImageShadow() {
        ivTop.layer.shadowColor = UIColor.black.cgColor
        ivTop.layer.shadowOffset = CGSize(width: 1, height: 1)
        ivTop.layer.shadowRadius = 4
        ivTop.layer.shadowOpacity = 0
        
        UIView.animate(withDuration: 0.6, delay: 1, options: .curveEaseOut, animations: {[weak self] in
            guard let sSelf = self else {
                return
            }
            sSelf.ivTop.layer.shadowOpacity = 0.4
        }, completion: nil)
        
        showView(vi: lblTitle) {[weak self] (_) in
            guard let sSelf = self else {
                return
            }
            sSelf.showView(vi: sSelf.lblSubTitle) { (_) in
                sSelf.showView(vi: sSelf.viRating) { (_) in
                    sSelf.showView(vi: sSelf.lblDescription) { (_) in
                        sSelf.btnMoreBottomConstraint.constant = -16
                        UIView.animate(withDuration: 0.5) {
                            sSelf.view.layoutIfNeeded()
                        }
                        sSelf.showView(vi: sSelf.btnMore) { (_) in
                        }
                    }
                }
            }
        }
        
    }
    
    private func showView(vi: UIView, completion: @escaping ((Bool)->Void)) {
        UIView.animate(withDuration: 0.3, animations: {
            vi.alpha = 1
        }, completion: completion)
    }
    
    func didSelect() {
        let revVC = ReviewVC()
        navigationController?.present(revVC, animated: true, completion: nil)
    }
    
    @objc private func closeBtnAction() {
//        navigationControsller?.popViewController(animated: true)
        let segue = DetailUnwindSegue(identifier: nil, source: self, destination: MainVC())
        segue.perform()
    }
    
    
    
}
